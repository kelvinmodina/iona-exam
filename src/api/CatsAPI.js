import APIUtils from "./APIUtils";

class CatsAPI {
    constructor() {
        this.apiutil = new APIUtils();
    }

    //handles listing of all cat breeds
    listBreeds = () => {
        try {
            let resp = this.apiutil.get('breeds');
            return resp;
        }
        catch (err) {
            return err;
        }
    }

    //handles listing of cats based on selected breed
    listCatsByBreed = (breed_id, page = 0, limit = 10) => {
        try {
            let resp = this.apiutil.get(`images/search?page=${page}&limit=${limit}&breed_id=${breed_id}&order=asc`);
            return resp;
        }
        catch (err) {
            return err;
        }
    }

    //handles displaying details of a seleced cat using id
    viewCatDetails = (breed_id) => {
        try {
            let resp = this.apiutil.get(`images/${breed_id}`);
            return resp;
        }
        catch (err) {
            return err;
        }
    }
}

export default CatsAPI;