import axios from "axios";

class APIUtils {
    constructor() {
        let service = axios.create({
            baseURL: 'https://api.thecatapi.com/v1',
            headers: {
                'x-api-key': process.env.REACT_APP_API_KEY
            }
        });
        this.service = service;
    }

    //API wrapper for handling GET requests
    get = (url) => {
        try {
            let resp = this.service.get(url);
            return resp;
        }
        catch (err) {
            return err;
        }
    }
}

export default APIUtils;