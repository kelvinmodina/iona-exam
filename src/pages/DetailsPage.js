import React, { Component } from "react";
import CatsAPI from "../api/CatsAPI";
import { Button, Container, Card, Alert } from "react-bootstrap";

class DetailsPage extends Component {
    constructor(props) {
        super(props);

        this.state = {
            id: props.match.params.id,
            details: {},
            img: "",
            hasError: false
        }
    }

    async componentDidMount() {
        try {
            let catsAPI = new CatsAPI();
            let resp = await catsAPI.viewCatDetails(this.state.id);

            if (resp.status === 200) {
                let details = resp.data.breeds[0]
                this.setState({ details: details });
                this.setState({ img: resp.data.url });
            }
            else {
                this.setState({ hasError: true })
            }
        }
        catch (err) {
            this.setState({ hasError: true });
        }
    }

    render() {
        let cat = this.state.details;
        let hasError = this.state.hasError;
        return (
            <Container>
                {
                    hasError
                        ? <Alert variant="danger">Apologies but we could not load new cats for you at this time! Miau!</Alert>
                        : ""
                }
                <Card>
                    <Card.Header>
                        <Button href={`/?breed=${cat.id}`}>Back</Button>
                    </Card.Header>
                    <Card.Img src={this.state.img} />
                    <Card.Body>
                        <h4>{cat.name}</h4>
                        <h5>Origin: {cat.origin}</h5>
                        <h6>{cat.temperament}</h6>
                        <p>{cat.description}</p>
                    </Card.Body>
                </Card>
            </Container>
        );
    }
}

export default DetailsPage;