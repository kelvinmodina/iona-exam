import React, { Component } from "react";
import CatsAPI from "../api/CatsAPI";
import { Button, Container, Row, Col, Card, Form, Alert } from "react-bootstrap";

class HomePage extends Component {
    constructor(props) {
        super(props);

        //query parameter handling
        const search = props.location.search;
        const breed = new URLSearchParams(search).get("breed"); //fetching query parameter value

        this.state = {
            breeds: [],
            catsListing: [],
            selectedCat: breed,
            page: 0,
            limit: 10,
            loadMore: {
                disabled: true,
                label: "Load More"
            },
            hasError: null
        };

        this.handleChange = this.handleChange.bind(this);
        this.renderList = this.renderList.bind(this);
        this.loadMore = this.loadMore.bind(this);
    }

    async componentDidMount() {
        let catsAPI = new CatsAPI();
        let breeds = await catsAPI.listBreeds();
        this.setState({ breeds: breeds.data });

        if (this.state.selectedCat) {
            this.renderList(this.state.selectedCat, 0);
        }
    }

    //Function that captures the event change when selecting value from breed dropdown
    handleChange(event) {
        let selectedBreed = event.target.value;
        this.setState({ selectedCat: selectedBreed, catsListing: [], page: 0 })
        this.renderList(selectedBreed, 0);
    }

    /*
    * Function that renders the list/result of cats from the selected breed
    * @param {string} selectedCat Id of the selected cat breed
    * @param {integer} page Page to be displayed, starting page is 0
    */
    async renderList(selectedCat, page = 0) {
        try {
            this.setState({ loadMore: { disabled: true, label: "Loading Cats..." } })

            let catsAPI = new CatsAPI();
            let resp = await catsAPI.listCatsByBreed(selectedCat, page, this.state.limit);

            if (resp.status === 200) {
                let catsList = [...new Set([...this.state.catsListing, ...resp.data])];

                this.setState({ catsListing: catsList });

                if (parseInt(resp.headers["pagination-count"]) !== catsList.length) {
                    this.setState({ loadMore: { disabled: false, label: "Load More" } })
                }
                else {
                    this.setState({ loadMore: { disabled: true, label: "Load More" } })
                }
            }
            else {
                this.setState({ error: true });
            }
        }
        catch (err) {
            this.setState({ error: true });
        }
    }

    //Function that handles the loading the next pagination of the result from the cat breed api
    loadMore() {
        let currentPage = this.state.page + 1;
        this.setState({ page: currentPage })
        this.renderList(this.state.selectedCat, currentPage);
    }

    render() {
        let hasError = this.state.hasError;
        let loadmore = this.state.loadMore;
        return (
            <Container>
                {
                    hasError
                        ? <Alert variant="danger">Apologies but we could not load new cats for you at this time! Miau!</Alert>
                        : ""
                }

                <h1>Cat Browser</h1>
                <Row>
                    <Col md={4}>
                        <Form>
                            <Form.Group>
                                <Form.Label>Breed</Form.Label>
                                <select className={"form-control"} onChange={this.handleChange} value={this.state.selectedCat}>
                                    <option>Select Breed</option>
                                    {this.state.breeds.map((element, index) => (
                                        <option key={element.id} value={element.id}>{element.name}</option>
                                    ))}
                                </select>
                            </Form.Group>
                        </Form>
                    </Col>
                </Row>
                <Row>
                    {this.state.catsListing.map((element, index) => (
                        <Col key={index} sm={6} md={3} style={{ paddingBottom: "10px" }}>
                            <Card>
                                <Card.Img src={element.url} />
                                <Card.Body>
                                    <Button href={`/${element.id}`}>View details</Button>
                                </Card.Body>
                            </Card>
                        </Col>
                    ))}
                </Row>
                <Button size="lg" variant="success" onClick={this.loadMore} disabled={loadmore.disabled}>
                    {loadmore.label}
                </Button>
            </Container>
        );
    }
}


export default HomePage;