import React from "react";
import {
  BrowserRouter as Router,
  Route
} from "react-router-dom";

import HomePage from './pages/HomePage.js';
import DetailsPage from './pages/DetailsPage.js';

function App() {
  return (
    <div className={"App"} style={{ padding: "20px" }}>
      <Router>
        <Route exact path="/" component={HomePage} />
        <Route path="/:id" component={DetailsPage} />
      </Router>
    </div>
  )
}

export default App;
